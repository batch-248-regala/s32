/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
	If the url is http://localhost:4000/, send a response Welcome to Booking System
	b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
	
	If the url is http://localhost:4000/courses, send a response Here's our courses available
	d. If the url is http://localhost:4000/addcourse, send a response Add a course to our
	resources

Create a simple server and the following routes with their corresponding HTTP methods and responses:
	e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources

Test all the endpoints in Postman.
*/

let http = require("http");



http.createServer(function(request,response){

  if(request.url === "/" && request.method === "GET"){

    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end(`Welcome to Booking System`);

  } else if(request.url === "/profile" && request.method === "GET"){

    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end(`Welcome to your profile`);

  } else if(request.url === "/courses" && request.method === "GET"){

    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end(`Here's our available courses`);

  } else if (request.url === "/addCourse" && request.method === "POST"){

    response.writeHead(200,{'Content-Type':'text/plain'});
    response.end(`Add course to our resources`);

  } else if (request.url === "/updateCourse" && request.method === "PUT"){

    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end(`Update a course to our resources`);

  } else if (request.url === "/archiveCourse" && request.method === "DELETE"){

    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end(`Archive course to our resources`);

  }

}).listen(4000);

console.log(`Server running at localhost:4000`);